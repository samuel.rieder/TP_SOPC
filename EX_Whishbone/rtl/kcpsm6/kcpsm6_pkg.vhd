--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package kcpsm6_pkg is

	component kcpsm6
		generic( 
			hwbuild : std_logic_vector(7 downto 0) := X"00";
			interrupt_vector : std_logic_vector(11 downto 0) := X"3FF";
			scratch_pad_memory_size : integer := 64
		);
		port ( 
			address : out std_logic_vector(11 downto 0);
			instruction : in std_logic_vector(17 downto 0);
			bram_enable : out std_logic;
			in_port : in std_logic_vector(7 downto 0);
			out_port : out std_logic_vector(7 downto 0);
			port_id : out std_logic_vector(7 downto 0);
			write_strobe : out std_logic;
			k_write_strobe : out std_logic;
			read_strobe : out std_logic;
			interrupt : in std_logic;
			interrupt_ack : out std_logic;
			sleep : in std_logic;
			reset : in std_logic;
			clk : in std_logic
		);
	end component;

	--BlockRAM
    component program_rom is
        generic(
            C_FAMILY : string := "S6"; 
            C_RAM_SIZE_KWORDS : integer := 1;
            C_JTAG_LOADER_ENABLE : integer := 0);
        Port (
            address : in std_logic_vector(11 downto 0);
            instruction : out std_logic_vector(17 downto 0);
            enable : in std_logic;
            rdl : out std_logic;                    
            clk : in std_logic);
    end component;

end kcpsm6_pkg;

package body kcpsm6_pkg is

---- Example 1
--  function <function_name>  (signal <signal_name> : in <type_declaration>  ) return <type_declaration> is
--    variable <variable_name>     : <type_declaration>;
--  begin
--    <variable_name> := <signal_name> xor <signal_name>;
--    return <variable_name>; 
--  end <function_name>;

---- Example 2
--  function <function_name>  (signal <signal_name> : in <type_declaration>;
--                         signal <signal_name>   : in <type_declaration>  ) return <type_declaration> is
--  begin
--    if (<signal_name> = '1') then
--      return <signal_name>;
--    else
--      return 'Z';
--    end if;
--  end <function_name>;

---- Procedure Example
--  procedure <procedure_name>  (<type_declaration> <constant_name>  : in <type_declaration>) is
--    
--  begin
--    
--  end <procedure_name>;
 
end kcpsm6_pkg;
