--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package uart_pkg is

	component uart 
	generic(
		-- Default setting:
		-- 19,200 baud, 8 data bis, 1 stop its, 2^2 FIFO
		DBIT: integer:=8;     -- # data bits
		SB_TICK: integer:=16; -- # ticks for stop bits, 16/24/32
									 --   for 1/1.5/2 stop bits
		DVSR: integer:= 163;  -- baud rate divisor
									 -- DVSR = 50M/(16*baud rate)
		DVSR_BIT: integer:=8; -- # bits of DVSR
		FIFO_W: integer:=2    -- # addr bits of FIFO
									 -- # words in FIFO=2^FIFO_W
	);
	port(
		clk, reset: in std_logic;
		rd_uart, wr_uart: in std_logic;
		rx: in std_logic;
		w_data: in std_logic_vector(7 downto 0);
		tx_full, rx_empty: out std_logic;
		r_data: out std_logic_vector(7 downto 0);
		tx: out std_logic
	);
	end component;

end uart_pkg;

package body uart_pkg is

---- Example 1
--  function <function_name>  (signal <signal_name> : in <type_declaration>  ) return <type_declaration> is
--    variable <variable_name>     : <type_declaration>;
--  begin
--    <variable_name> := <signal_name> xor <signal_name>;
--    return <variable_name>; 
--  end <function_name>;

---- Example 2
--  function <function_name>  (signal <signal_name> : in <type_declaration>;
--                         signal <signal_name>   : in <type_declaration>  ) return <type_declaration> is
--  begin
--    if (<signal_name> = '1') then
--      return <signal_name>;
--    else
--      return 'Z';
--    end if;
--  end <function_name>;

---- Procedure Example
--  procedure <procedure_name>  (<type_declaration> <constant_name>  : in <type_declaration>) is
--    
--  begin
--    
--  end <procedure_name>;
 
end uart_pkg;
