--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package wb_pkg is

component wbmaster32 
  port
    (
      clk_i   : in std_logic;
      rst_n_i : in std_logic;

      ---------------------------------------------------------
      -- Input
      pd_wbm_target_mrd_i : in std_logic;                      -- Target memory read
      pd_wbm_target_mwr_i : in std_logic;                      -- Target memory write

      pd_wbm_start_i : in std_logic;                      -- Address strobe
      pd_wbm_addr_i       : in std_logic_vector(31 downto 0);  -- Target address (in byte) that will increment with data
      pd_wbm_data_i       : in std_logic_vector(31 downto 0);  -- Data

      p_wr_rdy_o   : out std_logic_vector(1 downto 0);  -- Ready to accept target write
      p2l_rdy_o    : out std_logic;                     -- De-asserted to pause transfer already in progress

      ---------------------------------------------------------
      -- Output
		wb_data_read_o     : out std_logic_vector(31 downto 0);
		wb_data_read_rdy_o : out std_logic;

      ---------------------------------------------------------
      -- wishbone interface
      wb_clk_i   : in  std_logic;                      -- Wishbone bus clock
      wb_adr_o   : out std_logic_vector(30 downto 0);  -- Address
      wb_dat_o   : out std_logic_vector(31 downto 0);  -- Data out
      wb_sel_o   : out std_logic_vector(3 downto 0);   -- Byte select
      wb_stb_o   : out std_logic;                      -- Strobe
      wb_we_o    : out std_logic;                      -- Write
      wb_cyc_o   : out std_logic;                      -- Cycle
      wb_dat_i   : in  std_logic_vector(31 downto 0);  -- Data in
      wb_ack_i   : in  std_logic;                      -- Acknowledge
      wb_stall_i : in  std_logic                       -- Stall
      );
end component;

end wb_pkg;

package body wb_pkg is

---- Example 1
--  function <function_name>  (signal <signal_name> : in <type_declaration>  ) return <type_declaration> is
--    variable <variable_name>     : <type_declaration>;
--  begin
--    <variable_name> := <signal_name> xor <signal_name>;
--    return <variable_name>; 
--  end <function_name>;

---- Example 2
--  function <function_name>  (signal <signal_name> : in <type_declaration>;
--                         signal <signal_name>   : in <type_declaration>  ) return <type_declaration> is
--  begin
--    if (<signal_name> = '1') then
--      return <signal_name>;
--    else
--      return 'Z';
--    end if;
--  end <function_name>;

---- Procedure Example
--  procedure <procedure_name>  (<type_declaration> <constant_name>  : in <type_declaration>) is
--    
--  begin
--    
--  end <procedure_name>;
 
end wb_pkg;
