----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:45:48 02/05/2018 
-- Design Name: 
-- Module Name:    wb_top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

use work.chenillard_pkg.ALL;
use work.kcpsm6_pkg.ALL;
use work.uart_pkg.ALL;
use work.wb_pkg.ALL;

entity wb_top is
	Port ( 
		clk_i   : in  STD_LOGIC;
		reset_i : in  STD_LOGIC;
		
		serial_i : in std_logic;
		serial_o : out std_logic;
		
		port_o : out std_logic_vector(7 downto 0)
	);
end wb_top;

architecture Behavioral of wb_top is

	--Components
	component wb_chaser 
	  port (
		 rst_n_i                                  : in     std_logic;
		 clk_sys_i                                : in     std_logic;
		 wb_adr_i                                 : in     std_logic_vector(0 downto 0);
		 wb_dat_i                                 : in     std_logic_vector(31 downto 0);
		 wb_dat_o                                 : out    std_logic_vector(31 downto 0);
		 wb_cyc_i                                 : in     std_logic;
		 wb_sel_i                                 : in     std_logic_vector(3 downto 0);
		 wb_stb_i                                 : in     std_logic;
		 wb_we_i                                  : in     std_logic;
		 wb_ack_o                                 : out    std_logic;
		 wb_stall_o                               : out    std_logic;
		 -- Port for BIT field: 'Direction bit' in reg: 'Control Register'
		 reg_ctrl_dir_o                           : out    std_logic;
		 -- Port for BIT field: 'LED inversion bit' in reg: 'Control Register'
		 reg_ctrl_inv_o                           : out    std_logic;
		 -- Port for std_logic_vector field: 'Period' in reg: 'Speed of electronic chaser'
		 reg_speed_period_o                       : out    std_logic_vector(31 downto 0)
	  );
	end component;


	--Signals
	signal reset_n_s : std_logic;

	signal address : std_logic_vector(11 downto 0);
	signal instruction : std_logic_vector(17 downto 0);
	signal bram_enable : std_logic;
	signal in_port : std_logic_vector(7 downto 0);
	signal out_port : std_logic_vector(7 downto 0);
	signal port_id : std_logic_vector(7 downto 0);
	signal write_strobe : std_logic;
	signal k_write_strobe : std_logic;
	signal read_strobe : std_logic;
	signal interrupt : std_logic;
	signal interrupt_ack : std_logic;
	signal kcpsm6_sleep : std_logic;
	signal kcpsm6_reset : std_logic;
	
	signal rx_empty_s, tx_full_s : std_logic;
	signal rd_uart_s, wr_uart_s : std_logic;
	signal port_rd_s, port_wr_s: std_logic_vector(7 downto 0);

	signal port_addr_wb_s : std_logic_vector(31 downto 0);
	signal port_data_wb_s : std_logic_vector(31 downto 0);
	signal wr_addr_wb_fifo_s,wr_data_wb_fifo_s : std_logic;
	signal wb_nb_s : std_logic_vector(7 downto 0);
	signal wb_start_s : std_logic;
	signal pd_wbm_target_mrd_s, pd_wbm_target_mwr_s : std_logic;

	signal wb_data_read_s     : std_logic_vector(31 downto 0);
	signal wb_data_read_rdy_s : std_logic;
	signal led_s : std_logic_vector(7 downto 0);

   --Signaux du bus Wishbone 
	signal wb_adr_s            : std_logic_vector(30 downto 0);  -- Address
	signal wb_dat_out_master_s : std_logic_vector(31 downto 0);  -- Data out
	signal wb_sel_s            : std_logic_vector(3 downto 0);   -- Byte select
	signal wb_stb_s            : std_logic;                      -- Strobe
	signal wb_we_s             : std_logic;                      -- Write
	signal wb_cyc_s            : std_logic;                      -- Cycle
	signal wb_dat_in_master_s  : std_logic_vector(31 downto 0);  -- Data in
	signal wb_ack_s            : std_logic;                      -- Acknowledge
	signal wb_stall_s          : std_logic;                      -- Stall

	--Signaux pour connecter les registres
	signal reg_ctrl_dir_s : std_logic;                         -- Port for BIT field: 'Direction bit' in reg: 'Control Register'
	signal reg_ctrl_inv_s : std_logic;                         -- Port for BIT field: 'LED inversion bit' in reg: 'Control Register'
	signal reg_speed_period_s : std_logic_vector(31 downto 0); -- Port for std_logic_vector field: 'Period' in reg: 'Speed of electronic chaser'

begin

	--Reset
	reset_n_s <= not(reset_i);
	
----- KCPSM6 -----------------------------------------------------------------

	--Processor KCPSM6
	cmp_kcpsm6 : kcpsm6
	generic map (
		hwbuild => X"00",
		interrupt_vector => X"3FF",
		scratch_pad_memory_size => 64
	)
	port map(
		address        => address,
		instruction    => instruction,
		bram_enable    => bram_enable,
		port_id        => port_id,
		write_strobe   => write_strobe,
		k_write_strobe => k_write_strobe,
		out_port       => out_port,
		read_strobe    => read_strobe,
		in_port        => in_port,
		interrupt      => interrupt,
		interrupt_ack  => interrupt_ack,
		sleep          => kcpsm6_sleep,
		reset          => kcpsm6_reset,
		clk            => clk_i 
	);

	--Interrupt and sleep functions
	kcpsm6_sleep <= '0';
	--interrupt <= '0';	
	interrupt <= interrupt_ack;
	
	--BlockRAM
	cmp_program_rom: program_rom
	generic map(
		C_FAMILY             => "7S",
		C_RAM_SIZE_KWORDS    => 2,
		C_JTAG_LOADER_ENABLE => 1
	)
	port map(
		address     => address,
		instruction => instruction,
		enable      => bram_enable,
		rdl         => kcpsm6_reset,
		clk         => clk_i
	);
	
----- UART -------------------------------------------------------------------
	cmp_uart: uart
	generic map( 
		DBIT     => 8,  -- # data bits
		SB_TICK  => 16, -- # ticks for stop bits, 16/24/32
							 --   for 1/1.5/2 stop bits
		DVSR     => 325,-- baud rate divisor=100e6/(16*baudrate)	
		DVSR_BIT => 9,  -- # bits of DVSR
		FIFO_W   => 6   -- # addr bits of FIFO
							 -- # words in FIFO=2^FIFO_W
	)           
	port map(
		clk      => clk_i, 
		reset    => reset_i,
		
		rx       => serial_i,
		rd_uart  => rd_uart_s,
		r_data   => port_rd_s, 
		rx_empty => rx_empty_s,
		
		wr_uart  => wr_uart_s,  
		w_data   => port_wr_s,
		tx_full  => tx_full_s, 
		tx       => serial_o
	);	

	--Ouputs
	process(clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				port_wr_s  <= (others=>'0');
				wr_uart_s  <= '0';
				wb_nb_s    <= (others=>'0');
				port_addr_wb_s <= (others=>'0');
				port_data_wb_s <= (others=>'0');
			else
				wr_uart_s <= '0';
				wb_start_s <= '0'; --to have just one period clock to '1' when start is written
				if write_strobe = '1' then
					case port_id is
						when x"01" => 
							
						when x"02" => -- Output port UART
							port_wr_s <= out_port;
							wr_uart_s <= '1';
							
						when x"03" => -- Output port UART
							if (out_port(0)='0') then
								--wishbone read
								pd_wbm_target_mrd_s <= '1';
								pd_wbm_target_mwr_s <= '0';
							else
								--wishbone write
								pd_wbm_target_mrd_s <= '0';
								pd_wbm_target_mwr_s <= '1';						
							end if;
							
						when x"04" => -- addresse Wishbone
							case wb_nb_s is
								when x"00" =>
									port_addr_wb_s(7 downto 0) <= out_port;
								when x"01" =>
									port_addr_wb_s(15 downto 8) <= out_port;
								when x"02" =>
									port_addr_wb_s(23 downto 16) <= out_port;
								when x"03" =>
									port_addr_wb_s(31 downto 24) <= out_port;
									wr_addr_wb_fifo_s <= '1';
								when others => 
							end case;

						when x"05" => -- data Wishbone
							case wb_nb_s is
								when x"00" =>
									port_data_wb_s(7 downto 0) <= out_port;
								when x"01" =>
									port_data_wb_s(15 downto 8) <= out_port;
								when x"02" =>
									port_data_wb_s(23 downto 16) <= out_port;
								when x"03" =>
									port_data_wb_s(31 downto 24) <= out_port;
									wr_data_wb_fifo_s <= '1';
								when others => 
							end case;
							
						when x"06" => -- Wishbone byte selection
							wb_nb_s <= out_port;

						when x"07" => -- Wishbone start transaction
							wb_start_s <= out_port(0);
							
						when others => 
					end case;
				end if;
			end if;
		end if;
	end process;

	-- Read FIFO of UART when KCPSM6 read the port x"02"
	rd_uart_s <= '1' when port_id=x"02" and read_strobe='1' else '0';

	--Inputs
	--read-strobe is not used for input multiplexer (cf.p.73 KCPSM6_User_Guide.pdf)
	process(clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				in_port <= (others=>'0');
			else
				case port_id is
					when x"01" => 

					when x"02" => -- Input port UART
						in_port <= port_rd_s;
					when x"04" => -- UART control signal
						in_port <= "000000" & tx_full_s & rx_empty_s;
					when x"05" => -- UART control signal
						if (wb_data_read_rdy_s='1') then
							case wb_nb_s is
								when x"00" =>
									in_port <= wb_data_read_s(7 downto 0);
								when x"01" =>
									in_port <= wb_data_read_s(15 downto 8);
								when x"02" =>
									in_port <= wb_data_read_s(23 downto 16);
								when x"03" =>
									in_port <= wb_data_read_s(31 downto 24);
								when others => 
							end case;
						end if;
					when others => 
				end case;
			end if;
		end if;
	end process;


-----Wishbone - master --------------------------------------------------------------

	cmp_wbmaster32 : wbmaster32
		port map(
			---------------------------------------------------------
			-- GN4124 core clock and reset
			clk_i   => clk_i,
			rst_n_i => reset_n_s,

			---------------------------------------------------------
			-- Input
			pd_wbm_target_mrd_i => pd_wbm_target_mrd_s,                      -- Target memory read
			pd_wbm_target_mwr_i => pd_wbm_target_mwr_s,                      -- Target memory write

			pd_wbm_start_i => wb_start_s,                      -- Address strobe
			pd_wbm_addr_i       => port_addr_wb_s,  -- Target address (in byte) that will increment with data
			pd_wbm_data_i       => port_data_wb_s,  -- Data

			p_wr_rdy_o   => open,  -- Ready to accept target write
			p2l_rdy_o    => open,                     -- De-asserted to pause transfer already in progress

			---------------------------------------------------------
			-- Output
			wb_data_read_o     => wb_data_read_s,
			wb_data_read_rdy_o => wb_data_read_rdy_s,
			
			---------------------------------------------------------
			-- wishbone interface
			wb_clk_i   => clk_i,               -- Wishbone bus clock
			wb_adr_o   => wb_adr_s,            -- Address
			wb_dat_o   => wb_dat_out_master_s, -- Data out
			wb_sel_o   => wb_sel_s,            -- Byte select
			wb_stb_o   => wb_stb_s,            -- Strobe
			wb_we_o    => wb_we_s,             -- Write
			wb_cyc_o   => wb_cyc_s,            -- Cycle
			wb_dat_i   => wb_dat_in_master_s,  -- Data in
			wb_ack_i   => wb_ack_s,            -- Acknowledge
			wb_stall_i => wb_stall_s           -- Stall
		);

-----Wishbone - slave (registers) ----------------------------------------------------
	cmp_wb_chaser : wb_chaser
		port map(
			rst_n_i                                  => reset_n_s,
			clk_sys_i                                => clk_i,
			wb_adr_i                                 => wb_adr_s(0 downto 0),
			wb_dat_i                                 => wb_dat_out_master_s,
			wb_dat_o                                 => wb_dat_in_master_s,
			wb_cyc_i                                 => wb_cyc_s,
			wb_sel_i                                 => wb_sel_s,
			wb_stb_i                                 => wb_stb_s,
			wb_we_i                                  => wb_we_s,
			wb_ack_o                                 => wb_ack_s,
			wb_stall_o                               => wb_stall_s,
			-- Port for BIT field: 'Direction bit' in reg: 'Control Register'
			reg_ctrl_dir_o                           => reg_ctrl_dir_s,
			-- Port for BIT field: 'LED inversion bit' in reg: 'Control Register'
			reg_ctrl_inv_o                           => reg_ctrl_inv_s,
			-- Port for std_logic_vector field: 'Period' in reg: 'Speed of electronic chaser'
			reg_speed_period_o                       => reg_speed_period_s
		);

----- Chenillard ---------------------------------------------------------------------
	
	cmp_chenillard : chenillard
	port map( 
		clk_i   => clk_i,
		reset_i => reset_i,

		chg_dir_i => reg_ctrl_dir_s,   -- registre de direction
		period_i => reg_speed_period_s,-- registre de la p�riode
		
		port_o => led_s
	);
	
	--Inversion du port de sorti des LEDs en fonction du registre d'inversion
	port_o <= led_s when reg_ctrl_inv_s='0' else not(led_s);

end Behavioral;

