----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:13:38 03/22/2018 
-- Design Name: 
-- Module Name:    chenillard - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.chenillard_pkg.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity chenillard is
	port ( 
		clk_i   : in  std_logic;
		reset_i : in  std_logic;

		chg_dir_i : in std_logic;
		period_i  : in std_logic_vector(31 downto 0);
		
		port_o : out std_logic_vector(7 downto 0)
	);
end chenillard;

architecture Behavioral of chenillard is

	--Type
	type states_t is 
	(	
		IDLE,
		ON_RIGHT,
		ON_LEFT
	);

	signal state_s : states_t;

	signal led_s : unsigned(7 downto 0);
	signal counter_s : unsigned(31 downto 0);
	signal start_cnter_s, flag_counter_s : std_logic;
	
begin

	--Changement de type de chenillard
	process (clk_i) 
	begin
		if rising_edge(clk_i) then 
			if (reset_i='1') then
				state_s  <= IDLE;
				led_s <= "00000000";
				start_cnter_s <= '0';
			else
				--Default values
				start_cnter_s <= '0';
				case state_s is
					when IDLE =>
						if (chg_dir_i='1') then
							state_s  <= ON_RIGHT;
							led_s <= "00000001";
						else
							state_s  <= ON_LEFT;
							led_s <= "10000000";
						end if;
						
					when ON_RIGHT =>
						if (chg_dir_i='0') then
							state_s  <= ON_LEFT;
						end if;
						if (flag_counter_s='1') then
							led_s <= rotate_right(led_s, 1); 
						end if;
						start_cnter_s <= '1';
						
					when ON_LEFT =>
						if (chg_dir_i='1') then
							state_s  <= ON_RIGHT;
						end if;
						if (flag_counter_s='1') then
							led_s <= rotate_left(led_s, 1); 
						end if;
						start_cnter_s <= '1';
						
				end case;
			end if;
		end if;
	end process;
	
	--Output map
	port_o <= std_logic_vector(led_s);
	
	--Counter
	cmp_counter :  counter
	port map( 
		clk_i   => clk_i,
		reset_i => reset_i,

		start_i => start_cnter_s,
		period_i=> period_i,

		flag_o => flag_counter_s
	);

end Behavioral;

