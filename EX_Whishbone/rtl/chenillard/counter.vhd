----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:26:24 03/22/2018 
-- Design Name: 
-- Module Name:    counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter is
	Port ( 
		clk_i   : in  std_logic;
		reset_i : in  std_logic;

		start_i  : in std_logic;
		period_i : in std_logic_vector(31 downto 0);
		
		flag_o : out std_logic
	);
end counter;

architecture Behavioral of counter is

	--Signal 
	signal counter_s : unsigned(31 downto 0);

begin
	
	process(clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				counter_s <= (others=>'0');
				flag_o <= '0';
			else
				if (start_i='1') then
					counter_s <= counter_s + 1;
				end if;
				if (to_integer(counter_s)>=to_integer(unsigned(period_i))) then
					flag_o <= '1';
					counter_s <= (others=>'0');
				else
					flag_o <= '0';
				end if;
			end if;
		end if;
	end process;

end Behavioral;

