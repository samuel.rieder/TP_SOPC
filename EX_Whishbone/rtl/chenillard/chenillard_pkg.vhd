--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package chenillard_pkg is

	component counter 
		port ( 
			clk_i   : in  std_logic;
			reset_i : in  std_logic;

			start_i : in std_logic;
			period_i : in std_logic_vector(31 downto 0);
			
			flag_o : out std_logic
		);
	end component;

	component chenillard 
		port ( 
			clk_i   : in  std_logic;
			reset_i : in  std_logic;

			chg_dir_i : in std_logic;
			period_i  : in std_logic_vector(31 downto 0);
			
			port_o : out std_logic_vector(7 downto 0)
		);
	end component;

end chenillard_pkg;

package body chenillard_pkg is
 
end chenillard_pkg;
