--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:27:40 05/03/2018
-- Design Name:   
-- Module Name:   C:/Users/daniel.oberson/Desktop/Wishbone/wb - solution/TB_WB.vhd
-- Project Name:  wb
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: wb_top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
use work.chenillard_pkg.ALL;
use work.wb_pkg.ALL;
 
ENTITY TB_WB IS
END TB_WB;
 
ARCHITECTURE behavior OF TB_WB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
	component wb_chaser 
	  port (
		 rst_n_i                                  : in     std_logic;
		 clk_sys_i                                : in     std_logic;
		 wb_adr_i                                 : in     std_logic_vector(0 downto 0);
		 wb_dat_i                                 : in     std_logic_vector(31 downto 0);
		 wb_dat_o                                 : out    std_logic_vector(31 downto 0);
		 wb_cyc_i                                 : in     std_logic;
		 wb_sel_i                                 : in     std_logic_vector(3 downto 0);
		 wb_stb_i                                 : in     std_logic;
		 wb_we_i                                  : in     std_logic;
		 wb_ack_o                                 : out    std_logic;
		 wb_stall_o                               : out    std_logic;
		 -- Port for BIT field: 'Direction bit' in reg: 'Control Register'
		 reg_ctrl_dir_o                           : out    std_logic;
		 -- Port for BIT field: 'LED inversion bit' in reg: 'Control Register'
		 reg_ctrl_inv_o                           : out    std_logic;
		 -- Port for std_logic_vector field: 'Period' in reg: 'Speed of electronic chaser'
		 reg_speed_period_o                       : out    std_logic_vector(31 downto 0)
	  );
	end component;

   --Inputs
   signal sys_clk_s : std_logic := '0';
   signal reset_s,reset_n_s : std_logic := '0';

   -- Clock period definitions
   constant sys_clk_s_period : time := 10 ns;

	--Internal signals
	signal port_addr_wb_s : std_logic_vector(31 downto 0);
	signal port_data_wb_s : std_logic_vector(31 downto 0);
	signal wb_start_s : std_logic;
	signal pd_wbm_target_mrd_s, pd_wbm_target_mwr_s : std_logic;

	signal wb_adr_s            : std_logic_vector(30 downto 0);  -- Address
	signal wb_dat_out_master_s : std_logic_vector(31 downto 0);  -- Data out
	signal wb_sel_s            : std_logic_vector(3 downto 0);   -- Byte select
	signal wb_stb_s            : std_logic;                      -- Strobe
	signal wb_we_s             : std_logic;                      -- Write
	signal wb_cyc_s            : std_logic;                      -- Cycle
	signal wb_dat_in_master_s  : std_logic_vector(31 downto 0);  -- Data in
	signal wb_ack_s            : std_logic;                      -- Acknowledge
	signal wb_stall_s          : std_logic;                      -- Stall

	signal wb_data_read_s     : std_logic_vector(31 downto 0);
	signal wb_data_read_rdy_s : std_logic;

	signal reg_ctrl_dir_s : std_logic;                         -- Port for BIT field: 'Direction bit' in reg: 'Control Register'
	signal reg_ctrl_inv_s : std_logic;                         -- Port for BIT field: 'LED inversion bit' in reg: 'Control Register'
	signal reg_speed_period_s : std_logic_vector(31 downto 0); -- Port for std_logic_vector field: 'Period' in reg: 'Speed of electronic chaser'
	signal led_s : std_logic_vector(7 downto 0);

	signal port_led_o : std_logic_vector(7 downto 0);
	
BEGIN
 
-----Wishbone - master --------------------------------------------------------------

	cmp_wbmaster32 : wbmaster32
		port map(
			---------------------------------------------------------
			-- GN4124 core clock and reset
			clk_i   => sys_clk_s,
			rst_n_i => reset_n_s,

			---------------------------------------------------------
			-- Input
			pd_wbm_target_mrd_i => pd_wbm_target_mrd_s,                      -- Target memory read
			pd_wbm_target_mwr_i => pd_wbm_target_mwr_s,                      -- Target memory write

			pd_wbm_start_i => wb_start_s,                      -- Address strobe
			pd_wbm_addr_i       => port_addr_wb_s,  -- Target address (in byte) that will increment with data
			pd_wbm_data_i       => port_data_wb_s,  -- Data

			---------------------------------------------------------
			-- P2L channel control
			p_wr_rdy_o   => open,  -- Ready to accept target write
			p2l_rdy_o    => open,                     -- De-asserted to pause transfer already in progress

			---------------------------------------------------------
			-- Output
			wb_data_read_o     => wb_data_read_s,
			wb_data_read_rdy_o => wb_data_read_rdy_s,
			
			---------------------------------------------------------
			-- wishbone interface
			wb_clk_i   => sys_clk_s,                     -- Wishbone bus clock
			wb_adr_o   => wb_adr_s,  -- Address
			wb_dat_o   => wb_dat_out_master_s,  -- Data out
			wb_sel_o   => wb_sel_s,   -- Byte select
			wb_stb_o   => wb_stb_s,                    -- Strobe
			wb_we_o    => wb_we_s,                      -- Write
			wb_cyc_o   => wb_cyc_s,                      -- Cycle
			wb_dat_i   => wb_dat_in_master_s,  -- Data in
			wb_ack_i   => wb_ack_s,                      -- Acknowledge
			wb_stall_i => wb_stall_s                       -- Stall
		);

-----Wishbone - slave (registers) ----------------------------------------------------
	cmp_wb_chaser : wb_chaser
		port map(
			rst_n_i                                  => reset_n_s,
			clk_sys_i                                => sys_clk_s,
			wb_adr_i                                 => wb_adr_s(0 downto 0),
			wb_dat_i                                 => wb_dat_out_master_s,
			wb_dat_o                                 => wb_dat_in_master_s,
			wb_cyc_i                                 => wb_cyc_s,
			wb_sel_i                                 => wb_sel_s,
			wb_stb_i                                 => wb_stb_s,
			wb_we_i                                  => wb_we_s,
			wb_ack_o                                 => wb_ack_s,
			wb_stall_o                               => wb_stall_s,
			-- Port for BIT field: 'Direction bit' in reg: 'Control Register'
			reg_ctrl_dir_o                           => reg_ctrl_dir_s,
			-- Port for BIT field: 'LED inversion bit' in reg: 'Control Register'
			reg_ctrl_inv_o                           => reg_ctrl_inv_s,
			-- Port for std_logic_vector field: 'Period' in reg: 'Speed of electronic chaser'
			reg_speed_period_o                       => reg_speed_period_s
		);

----- Chenillard ---------------------------------------------------------------------
	
	cmp_chenillard : chenillard
	port map( 
		clk_i   => sys_clk_s,
		reset_i => reset_s,

		chg_dir_i => reg_ctrl_dir_s,
		period_i => reg_speed_period_s,
		
		port_o => led_s
	);
	
	port_led_o <= led_s when reg_ctrl_inv_s='0' else not(led_s);
	
	reset_n_s <= not(reset_s);
	
   -- Clock process definitions
   sys_clk_i_process :process
   begin
		sys_clk_s <= '0';
		wait for sys_clk_s_period/2;
		sys_clk_s <= '1';
		wait for sys_clk_s_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		port_addr_wb_s <= x"00000000";
		port_data_wb_s <= x"00000000";
		pd_wbm_target_mrd_s <= '0';
		pd_wbm_target_mwr_s <= '0';
		wb_start_s <= '0';
      reset_s <= '1';
		wait for 100 ns;	
      reset_s <= '0';

      wait for sys_clk_s_period*10;
		
																							
		--write
		port_addr_wb_s <= x"00000000";
		port_data_wb_s <= x"00000002";
		pd_wbm_target_mrd_s <= '0';
		pd_wbm_target_mwr_s <= '1';
		wb_start_s <= '1';
      wait for sys_clk_s_period;
 		pd_wbm_target_mrd_s <= '0';
		pd_wbm_target_mwr_s <= '0';
		wb_start_s <= '0';
		wait for sys_clk_s_period*40;

		--read
		port_addr_wb_s <= x"00000000";
		port_data_wb_s <= x"00000000";
		pd_wbm_target_mrd_s <= '1';
		pd_wbm_target_mwr_s <= '0';
		wb_start_s <= '1';
      wait for sys_clk_s_period;
		pd_wbm_target_mrd_s <= '0';
		pd_wbm_target_mwr_s <= '0';
		wb_start_s <= '0';
      wait for sys_clk_s_period*40;

		--write
		port_addr_wb_s <= x"00000004";
		port_data_wb_s <= x"00000001";
		pd_wbm_target_mrd_s <= '0';
		pd_wbm_target_mwr_s <= '1';
		wb_start_s <= '1';
      wait for sys_clk_s_period;
 		pd_wbm_target_mrd_s <= '0';
		pd_wbm_target_mwr_s <= '0';
		wb_start_s <= '0';
		wait for sys_clk_s_period*40;

		--read
		port_addr_wb_s <= x"00000004";
		port_data_wb_s <= x"00000000";
		pd_wbm_target_mrd_s <= '1';
		pd_wbm_target_mwr_s <= '0';
		wb_start_s <= '1';
      wait for sys_clk_s_period;
		pd_wbm_target_mrd_s <= '0';
		pd_wbm_target_mwr_s <= '0';
		wb_start_s <= '0';
      wait for sys_clk_s_period*40;

		--write
		port_addr_wb_s <= x"00000000";
		port_data_wb_s <= x"00000001";
		pd_wbm_target_mrd_s <= '0';
		pd_wbm_target_mwr_s <= '1';
		wb_start_s <= '1';
      wait for sys_clk_s_period;
 		pd_wbm_target_mrd_s <= '0';
		pd_wbm_target_mwr_s <= '0';
		wb_start_s <= '0';
		wait for sys_clk_s_period*40;

		--read
		port_addr_wb_s <= x"00000000";
		port_data_wb_s <= x"00000000";
		pd_wbm_target_mrd_s <= '1';
		pd_wbm_target_mwr_s <= '0';
		wb_start_s <= '1';
      wait for sys_clk_s_period;
		pd_wbm_target_mrd_s <= '0';
		pd_wbm_target_mwr_s <= '0';
		wb_start_s <= '0';
      wait for sys_clk_s_period*40;

		--write
		port_addr_wb_s <= x"00000004";
		port_data_wb_s <= x"12345678";
		pd_wbm_target_mrd_s <= '0';
		pd_wbm_target_mwr_s <= '1';
		wb_start_s <= '1';
      wait for sys_clk_s_period;
 		pd_wbm_target_mrd_s <= '0';
		pd_wbm_target_mwr_s <= '0';
		wb_start_s <= '0';
		wait for sys_clk_s_period*40;

		--read
		port_addr_wb_s <= x"00000004";
		port_data_wb_s <= x"00000000";
		pd_wbm_target_mrd_s <= '1';
		pd_wbm_target_mwr_s <= '0';
		wb_start_s <= '1';
      wait for sys_clk_s_period;
		pd_wbm_target_mrd_s <= '0';
		pd_wbm_target_mwr_s <= '0';
		wb_start_s <= '0';
      wait for sys_clk_s_period*40;
		
      wait;
   end process;

END;
