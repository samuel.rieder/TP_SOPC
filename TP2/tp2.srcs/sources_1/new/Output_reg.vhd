----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07.03.2019 14:52:42
-- Design Name: 
-- Module Name: Output_reg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Output_reg is
    Port ( data_i : in STD_LOGIC_VECTOR (7 downto 0);
           portid_i : in STD_LOGIC_VECTOR (7 downto 0);
           clk_i : in STD_LOGIC;
           reset_i : in STD_LOGIC;
           write_strobe_i : in STD_LOGIC;
           data1_o : out STD_LOGIC_VECTOR (7 downto 0);
           data2_o : out STD_LOGIC_VECTOR (7 downto 0));
end Output_reg;

architecture Behavioral of Output_reg is

begin
 process(clk_i, reset_i)
   begin
       if reset_i = '1' then 
           data1_o <= (others => '0');
           data2_o <= (others => '0');
       elsif rising_edge(clk_i) then
           if write_strobe_i = '1' then
                case portid_i is
                   when x"01" =>
                       data1_o <= data_i;
				   when x"02" =>
					   data2_o <= data_i;
                   when others =>
                       data1_o <= (others=>'0');
					   data2_o <= (others=>'0');
                   end case;
           end if;
       end if;
end process;

end Behavioral;
