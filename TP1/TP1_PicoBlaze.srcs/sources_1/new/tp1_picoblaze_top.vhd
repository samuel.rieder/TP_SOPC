----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07.03.2019 13:38:26
-- Design Name: 
-- Module Name: tp1_picoblaze_top - Structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tp1_picoblaze_top is
    Port ( clk_i   : in STD_LOGIC;
           reset_i : in STD_LOGIC;
           port1_i : in STD_LOGIC_VECTOR (7 downto 0);
           port1_o : out STD_LOGIC_VECTOR (7 downto 0));
end tp1_picoblaze_top;

architecture Structural of tp1_picoblaze_top is

	component kcpsm6 
		generic(                 hwbuild : std_logic_vector(7 downto 0) := X"00";
						interrupt_vector : std_logic_vector(11 downto 0) := X"3FF";
				 scratch_pad_memory_size : integer := 64);
		port(	                 address : out std_logic_vector(11 downto 0);
							 instruction : in std_logic_vector(17 downto 0);
							 bram_enable : out std_logic;
								 in_port : in std_logic_vector(7 downto 0);
								out_port : out std_logic_vector(7 downto 0);
								 port_id : out std_logic_vector(7 downto 0);
							write_strobe : out std_logic;
						  k_write_strobe : out std_logic;
							 read_strobe : out std_logic;
							   interrupt : in std_logic;
						   interrupt_ack : out std_logic;
								   sleep : in std_logic;
								   reset : in std_logic;
									 clk : in std_logic
			);
	end component;
  
	component TP1_picoblaze                            
		generic(             C_FAMILY : string := "7S"; 
					C_RAM_SIZE_KWORDS : integer := 1;
				 C_JTAG_LOADER_ENABLE : integer := 1);
		Port(
					address : in std_logic_vector(11 downto 0);
				instruction : out std_logic_vector(17 downto 0);
					 enable : in std_logic;
						rdl : out std_logic;                    
						clk : in std_logic
			);
	end component;

	component Input_reg
		Port ( data_i   : in STD_LOGIC_VECTOR (7 downto 0);
			   clk_i    : in STD_LOGIC;
			   reset_i  : in STD_LOGIC;
			   data_o   : out STD_LOGIC_VECTOR (7 downto 0);
			   portid_i : in STD_LOGIC_VECTOR (7 downto 0));
	end component;

	component Output_reg
		Port ( data_i : in STD_LOGIC_VECTOR (7 downto 0);
			   portid_i : in STD_LOGIC_VECTOR (7 downto 0);
			   clk_i : in STD_LOGIC;
			   reset_i : in STD_LOGIC;
			   write_strobe_i : in STD_LOGIC;
			   data_o : out STD_LOGIC_VECTOR (7 downto 0));
	end component;

	signal         address : std_logic_vector(11 downto 0);
	signal     instruction : std_logic_vector(17 downto 0);
	signal     bram_enable : std_logic;
	signal         in_port : std_logic_vector(7 downto 0);
	signal        out_port : std_logic_vector(7 downto 0);
	signal         port_id : std_logic_vector(7 downto 0);
	signal    write_strobe : std_logic;
	signal  k_write_strobe : std_logic;
	signal     read_strobe : std_logic;
	signal       interrupt : std_logic;
	signal   interrupt_ack : std_logic;
	signal    kcpsm6_sleep : std_logic;
	signal    kcpsm6_reset : std_logic;
begin
processor: kcpsm6
generic map (                        hwbuild => X"00", 
                            interrupt_vector => X"3FF",
                     scratch_pad_memory_size => 64)
port map(      address => address,
         instruction => instruction,
         bram_enable => bram_enable,
             port_id => port_id,
        write_strobe => write_strobe,
      k_write_strobe => k_write_strobe,
            out_port => out_port,
         read_strobe => read_strobe,
             in_port => in_port,
           interrupt => '0',
       interrupt_ack => interrupt_ack,
               sleep => '0',
               reset => reset_i,
                 clk => clk_i);
                 
   program_rom: TP1_picoblaze                  
   generic map(             C_FAMILY => "7S",   --Family 'S6', 'V6' or '7S'
                   C_RAM_SIZE_KWORDS => 2,      --Program size '1', '2' or '4'
                C_JTAG_LOADER_ENABLE => 1)      --Include JTAG Loader when set to '1' 
   port map(      address => address,      
              instruction => instruction,
                   enable => bram_enable,
                      rdl => kcpsm6_reset,
                      clk => clk_i);


    reg_in: Input_reg
        port map(
                clk_i    => clk_i,
                reset_i  => reset_i,
                portid_i => port_id,
                data_i   => port1_i,
                data_o   => in_port
                );
        
     reg_out: Output_reg
        port map(
                clk_i => clk_i,
                write_strobe_i => write_strobe,
                reset_i => reset_i,
                portid_i => port_id,
                data_i => out_port,
                data_o => port1_o
                );

end Structural;
